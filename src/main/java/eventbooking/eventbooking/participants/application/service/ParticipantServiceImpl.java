package eventbooking.eventbooking.participants.application.service;

import eventbooking.eventbooking.participants.application.exception.InvalidInputException;
import eventbooking.eventbooking.participants.domain.model.Business;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.ParticipantDTO;
import eventbooking.eventbooking.participants.domain.model.Person;
import eventbooking.eventbooking.participants.domain.repository.ParticipantRepository;
import eventbooking.eventbooking.participants.domain.service.ParticipantService;
import eventbooking.eventbooking.payment.domain.info.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParticipantServiceImpl implements ParticipantService {

    ParticipantRepository repository;

    @Autowired
    public ParticipantServiceImpl(ParticipantRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(ParticipantDTO participantDTO) {
        if (participantDTO.isBusiness()) {
            saveBusiness(participantDTO);
        } else {
            savePerson(participantDTO);
        }
    }

    private void savePerson(ParticipantDTO participantDTO) {
        if (participantDTO.getFirst() == null || participantDTO.getSecond() == null || participantDTO.getEventId() == null ||
                participantDTO.getThird() == null || participantDTO.getMethod() == null || participantDTO.getInfo() == null) {
            throw new InvalidInputException("Kohustulik sisend on puudu.");
        }

        if (participantDTO.getFirst().isEmpty() || participantDTO.getSecond().isEmpty() ||
                participantDTO.getEventId().isEmpty() || participantDTO.getThird().isEmpty() ||
                participantDTO.getMethod().isEmpty()) {
            throw new InvalidInputException("Kohustulik sisend on tühi.");
        }

        Person person = convertToPerson(participantDTO);


        if (person.getFirstname().length() > 50 || person.getLastname().length() > 50) {
            throw new InvalidInputException("Eesnime ja perenime maksimaalne pikkus on 50.");
        }

        if (person.getInfo().length() > 1500) {
            throw new InvalidInputException("Info maksimaalne pikkus on 1500");
        }

        repository.savePerson(person);
    }

    private Person convertToPerson(ParticipantDTO participantDTO) {
        Person person = new Person();

        person.setFirstname(participantDTO.getFirst());
        person.setLastname(participantDTO.getSecond());
        person.setInfo(participantDTO.getInfo());

        try {
            Integer code = Integer.parseInt(participantDTO.getThird());
            person.setCode(code);

            Method method = Method.valueOf(participantDTO.getMethod().toUpperCase());
            person.setMethod(method);

            Integer eventId = Integer.parseInt(participantDTO.getEventId());
            person.setEventId(eventId);
        } catch (Exception e) {
            throw new InvalidInputException("Mittesobiv sisend.");
        }

        return person;
    }

    private void saveBusiness(ParticipantDTO participantDTO) {
        Business business = convertToBusiness(participantDTO);

        if (business.getName() == null || business.getCode() == null || business.getEventId() == null ||
                business.getParticipants() == null || business.getMethod() == null || business.getInfo() == null) {
            throw new InvalidInputException("Kohustulik sisend on puudu.");
        }

        if (business.getName().length() > 100) {
            throw new InvalidInputException("Name maksimaalne pikkus on 100.");
        }

        if (business.getInfo().length() > 5000) {
            throw new InvalidInputException("Info maksimaalne pikkus on 5000");
        }

        repository.saveBusiness(business);
    }

    private Business convertToBusiness(ParticipantDTO participantDTO) {
        Business business = new Business();

        business.setName(participantDTO.getFirst());
        business.setInfo(participantDTO.getInfo());

        try {
            Method method = Method.valueOf(participantDTO.getMethod().toUpperCase());
            business.setMethod(method);

            Integer code = Integer.parseInt(participantDTO.getSecond());
            business.setCode(code);

            Integer participants = Integer.parseInt(participantDTO.getThird());
            business.setParticipants(participants);

            Integer eventId = Integer.parseInt(participantDTO.getEventId());
            business.setEventId(eventId);
        } catch (Exception e) {
            throw new InvalidInputException("Mittesobiv sisend.");
        }

        return business;
    }

    @Override
    public List<Participant> getParticipants(int eventId) {
        List<Person> people = repository.allPeople(eventId);
        people.forEach(person -> person.setBusiness(false));
        List<Participant> participants = new ArrayList<>(people);

        List<Business> businesses = repository.allBusinesses(eventId);
        businesses.forEach(business -> business.setBusiness(true));
        participants.addAll(businesses);

        return participants;
    }

    @Override
    public void removePerson(int personId) {
        repository.removePerson(personId);
    }

    @Override
    public void removeBusiness(int businessId) {
        repository.removeBusiness(businessId);
    }

    @Override
    public Participant getPersonById(int participantId) {
        return repository.getPersonById(participantId);
    }

    @Override
    public void updateParticipant(ParticipantDTO participant) {
        if (!participant.isBusiness()) {
            Person person = convertToPerson(participant);
            person.setId(Integer.parseInt(participant.getId()));
            repository.updatePerson(person);
        } else {
            Business business = convertToBusiness(participant);
            business.setId(Integer.parseInt(participant.getId()));
            repository.updateBusiness(business);
        }
    }

    @Override
    public Participant getBusinessById(int participantId) {
        Business business = repository.getBusinessById(participantId);
        business.setBusiness(true);
        return business;
    }
}
