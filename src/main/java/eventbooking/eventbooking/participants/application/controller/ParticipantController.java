package eventbooking.eventbooking.participants.application.controller;

import eventbooking.eventbooking.participants.application.exception.InvalidInputException;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.ParticipantDTO;
import eventbooking.eventbooking.participants.domain.service.ParticipantService;
import eventbooking.eventbooking.payment.domain.service.PaymentMethodService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ParticipantController {

    private final @NonNull ParticipantService service;
    private final @NonNull PaymentMethodService paymentMethodService;

    @Autowired
    public ParticipantController(@NonNull ParticipantService service, @NonNull PaymentMethodService paymentMethodService) {
        this.service = service;
        this.paymentMethodService = paymentMethodService;
    }

    @PostMapping("/add_participant")
    public ResponseEntity<String> addParticipant(@ModelAttribute ParticipantDTO participant) {
        try {
            service.save(participant);

            return new ResponseEntity<>("Added new participant", HttpStatus.OK);
        } catch (InvalidInputException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/remove_person/{personId}")
    public void removeEvent(@PathVariable int personId) {
        service.removePerson(personId);
    }

    @DeleteMapping("/remove_business/{businessId}")
    public void removeBusiness(@PathVariable int businessId) {
        service.removeBusiness(businessId);
    }

    @GetMapping("/person/{participantId}")
    public String personPage(@PathVariable int participantId, Model model) {
        Participant participant = service.getPersonById(participantId);

        model.addAttribute("participant", participant);
        model.addAttribute("methods", paymentMethodService.all());

        return "participant";
    }

    @GetMapping("/business/{participantId}")
    public String businessPage(@PathVariable int participantId, Model model) {
        Participant participant = service.getBusinessById(participantId);

        model.addAttribute("participant", participant);
        model.addAttribute("methods", paymentMethodService.all());

        return "participant";
    }

    @PutMapping("/update_participant")
    public ResponseEntity<String> updateParticipant(@ModelAttribute ParticipantDTO participant) {
        try {
            service.updateParticipant(participant);

            return new ResponseEntity<>("Updated participant", HttpStatus.OK);
        } catch (InvalidInputException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
