package eventbooking.eventbooking.participants.domain.model;

import lombok.Data;

@Data
public class Person extends Participant {
    private String firstname;
    private String lastname;
    private Integer code;
}
