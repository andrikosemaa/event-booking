package eventbooking.eventbooking.participants.domain.model;

import lombok.Data;

@Data
public class ParticipantDTO {
    private String id;
    private String eventId;
    private String first;
    private String second;
    private String third;
    private String method;
    private String info;
    private boolean business;
}
