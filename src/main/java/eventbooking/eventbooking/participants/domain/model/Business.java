package eventbooking.eventbooking.participants.domain.model;

import lombok.Data;

@Data
public class Business extends Participant {
    private String name;
    private Integer code;
    private Integer participants;
}
