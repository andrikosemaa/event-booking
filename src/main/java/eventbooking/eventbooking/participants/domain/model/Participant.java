package eventbooking.eventbooking.participants.domain.model;

import eventbooking.eventbooking.payment.domain.info.Method;
import lombok.Data;

@Data
public class Participant {
    private Integer id;
    private Integer eventId;
    private Method method;
    private String info;
    private boolean business;
}
