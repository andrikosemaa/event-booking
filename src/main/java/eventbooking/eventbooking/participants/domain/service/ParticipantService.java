package eventbooking.eventbooking.participants.domain.service;

import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.ParticipantDTO;

import java.util.List;

public interface ParticipantService {

    void save(ParticipantDTO participant);

    List<Participant> getParticipants(int eventId);

    void removePerson(int personId);

    void removeBusiness(int businessId);

    Participant getPersonById(int participantId);

    void updateParticipant(ParticipantDTO participant);

    Participant getBusinessById(int participantId);
}
