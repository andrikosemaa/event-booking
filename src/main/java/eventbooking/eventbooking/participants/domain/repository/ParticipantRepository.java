package eventbooking.eventbooking.participants.domain.repository;

import eventbooking.eventbooking.participants.domain.model.Business;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.Person;

import java.util.List;

public interface ParticipantRepository {

    void savePerson(Person person);

    void saveBusiness(Business business);

    void removeParticipantsByEventId(int eventId);

    List<Person> allPeople(int eventId);

    List<Business> allBusinesses(int eventId);

    void removePerson(int personId);

    void removeBusiness(int businessId);

    Participant getPersonById(int personId);

    void updatePerson(Person person);

    Business getBusinessById(int businessId);

    void updateBusiness(Business business);
}
