package eventbooking.eventbooking.participants.infrastructure;

import eventbooking.eventbooking.participants.domain.model.Business;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.Person;
import eventbooking.eventbooking.participants.domain.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParticipantRepositoryImpl implements ParticipantRepository {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public ParticipantRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void savePerson(Person person) {
        jdbcTemplate.update("INSERT INTO people (firstname, lastname, code, method, info, event_id)" +
                        "VALUES(?,?,?,?,?,?)",
                new Object[]{person.getFirstname(), person.getLastname(), person.getCode(),
                        person.getMethod().toString(), person.getInfo(), person.getEventId()});
    }

    @Override
    public void saveBusiness(Business business) {
        jdbcTemplate.update("INSERT INTO businesses (name, code, participants, method, info, event_id)" +
                        "VALUES(?,?,?,?,?,?)",
                new Object[]{business.getName(), business.getCode(), business.getParticipants(),
                        business.getMethod(), business.getInfo(), business.getEventId()});
    }

    @Override
    public void removeParticipantsByEventId(int eventId) {
        jdbcTemplate.update("DELETE FROM people WHERE id=?", eventId);
        jdbcTemplate.update("DELETE FROM businesses WHERE id=?", eventId);
    }

    @Override
    public List<Person> allPeople(int eventId) {
        return jdbcTemplate.query("SELECT * FROM people WHERE event_id=?",
                BeanPropertyRowMapper.newInstance(Person.class), eventId);
    }

    @Override
    public List<Business> allBusinesses(int eventId) {
        return jdbcTemplate.query("SELECT * FROM businesses WHERE event_id=?",
                BeanPropertyRowMapper.newInstance(Business.class), eventId);
    }

    @Override
    public void removePerson(int personId) {
        jdbcTemplate.update("DELETE FROM people WHERE id=?", personId);
    }

    @Override
    public void removeBusiness(int businessId) {
        jdbcTemplate.update("DELETE FROM businesses WHERE id=?", businessId);
    }

    @Override
    public Participant getPersonById(int personId) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM people WHERE id=?",
                    BeanPropertyRowMapper.newInstance(Person.class), personId);
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    @Override
    public void updatePerson(Person person) {
        jdbcTemplate.update("UPDATE people SET firstname=?, lastname=?, code=?, method=?, info=? WHERE id=?",
                person.getFirstname(), person.getLastname(), person.getCode(),
                person.getMethod().toString(), person.getInfo(), person.getId());
    }

    @Override
    public Business getBusinessById(int businessId) {
        return jdbcTemplate.queryForObject("SELECT * FROM businesses WHERE id=?",
                BeanPropertyRowMapper.newInstance(Business.class), businessId);
    }

    @Override
    public void updateBusiness(Business business) {
        jdbcTemplate.update("UPDATE businesses SET name = ?, code = ?, participants = ?, method = ?, info = ? WHERE id = ?",
                business.getName(), business.getCode(), business.getParticipants(),
                business.getMethod(), business.getInfo(), business.getId());
    }
}
