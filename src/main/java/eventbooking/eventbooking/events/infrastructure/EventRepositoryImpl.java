package eventbooking.eventbooking.events.infrastructure;

import eventbooking.eventbooking.events.domain.model.Event;
import eventbooking.eventbooking.events.domain.repository.EventRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventRepositoryImpl implements EventRepository {

    private final @NonNull JdbcTemplate jdbcTemplate;

    @Autowired
    public EventRepositoryImpl(@NonNull JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(Event event) {
        jdbcTemplate.update("INSERT INTO events (name, time, place, info)" +
                        "VALUES(?,?,?,?)",
                new Object[]{event.getName(), event.getTime().toString(), event.getPlace(), event.getInfo()});
    }

    @Override
    public Event findById(int eventId) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM events WHERE id=?",
                    BeanPropertyRowMapper.newInstance(Event.class), eventId);
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Event> all() {
        return jdbcTemplate.query("SELECT * FROM events", BeanPropertyRowMapper.newInstance(Event.class));
    }

    @Override
    public void removeEvent(int eventId) {
        jdbcTemplate.update("DELETE FROM events WHERE id=?", eventId);
    }
}
