package eventbooking.eventbooking.events.domain.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Event {
    private int id;
    private String name;
    private LocalDateTime time;
    private String place;
    private String info;
    private String displayTime;
    private Integer count;
}
