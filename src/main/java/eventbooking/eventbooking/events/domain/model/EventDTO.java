package eventbooking.eventbooking.events.domain.model;

import lombok.Data;

@Data
public class EventDTO {
    private String name;
    private String time;
    private String place;
    private String info;
}
