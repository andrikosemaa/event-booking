package eventbooking.eventbooking.events.domain.repository;

import eventbooking.eventbooking.events.domain.model.Event;

import java.util.List;

public interface EventRepository {

    void save(Event event);

    List<Event> all();

    Event findById(int eventId);

    void removeEvent(int eventId);
}
