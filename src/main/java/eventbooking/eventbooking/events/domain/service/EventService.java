package eventbooking.eventbooking.events.domain.service;

import eventbooking.eventbooking.events.domain.model.Event;
import eventbooking.eventbooking.events.domain.model.EventDTO;

import java.util.List;

public interface EventService {

    void addEvent(EventDTO eventDTO);

    List<Event> all();

    List<Event> futureEvents();

    List<Event> pastEvents();

    Event findById(int eventId);

    void removeEvent(int eventId);
}
