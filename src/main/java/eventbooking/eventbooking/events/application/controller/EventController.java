package eventbooking.eventbooking.events.application.controller;

import eventbooking.eventbooking.events.domain.model.Event;
import eventbooking.eventbooking.events.domain.model.EventDTO;
import eventbooking.eventbooking.events.domain.service.EventService;
import eventbooking.eventbooking.participants.application.exception.InvalidInputException;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.service.ParticipantService;
import eventbooking.eventbooking.payment.domain.service.PaymentMethodService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EventController {

    private final @NonNull EventService service;
    private final @NonNull ParticipantService participantService;
    private final @NonNull PaymentMethodService paymentMethodService;

    @Autowired
    public EventController(@NonNull EventService service, @NonNull ParticipantService participantService, @NonNull PaymentMethodService paymentMethodService) {
        this.service = service;
        this.participantService = participantService;
        this.paymentMethodService = paymentMethodService;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("futureEvents", service.futureEvents());
        model.addAttribute("pastEvents", service.pastEvents());

        return "index";
    }

    @GetMapping("/add_event")
    public String addEventPage(Model model) {
        return "add_event";
    }

    @GetMapping("/event/{eventId}")
    public String eventPage(@PathVariable int eventId, Model model) {
        Event event = service.findById(eventId);
        List<Participant> participants = participantService.getParticipants(eventId);

        model.addAttribute("participants", participants);
        model.addAttribute("event", event);
        model.addAttribute("methods", paymentMethodService.all());

        return "event";
    }

    @DeleteMapping("/remove_event/{eventId}")
    public void removeEvent(@PathVariable int eventId, Model model) {
        service.removeEvent(eventId);
    }

    @PostMapping("/add_event")
    public ResponseEntity<String> newEvent(@ModelAttribute EventDTO event) {
        try {
            service.addEvent(event);

            return new ResponseEntity<>("Added new event", HttpStatus.OK);
        } catch (InvalidInputException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
