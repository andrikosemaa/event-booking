package eventbooking.eventbooking.events.application.service;

import eventbooking.eventbooking.events.domain.model.Event;
import eventbooking.eventbooking.events.domain.model.EventDTO;
import eventbooking.eventbooking.events.domain.repository.EventRepository;
import eventbooking.eventbooking.events.domain.service.EventService;
import eventbooking.eventbooking.participants.application.exception.InvalidInputException;
import eventbooking.eventbooking.participants.domain.model.Business;
import eventbooking.eventbooking.participants.domain.model.Participant;
import eventbooking.eventbooking.participants.domain.model.Person;
import eventbooking.eventbooking.participants.domain.repository.ParticipantRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private final @NonNull EventRepository repository;
    private final @NonNull ParticipantRepository participantRepository;

    @Autowired
    public EventServiceImpl(@NonNull EventRepository repository, @NonNull ParticipantRepository participantRepository) {
        this.repository = repository;
        this.participantRepository = participantRepository;
    }

    @Override
    public void addEvent(EventDTO eventDTO) {
        if (eventDTO.getName() == null || eventDTO.getTime() == null || eventDTO.getPlace() == null) {
            throw new InvalidInputException("Kohustuslik väli on puudu.");
        }

        if (eventDTO.getName().isEmpty() || eventDTO.getTime().isEmpty() || eventDTO.getPlace().isEmpty()) {
            throw new InvalidInputException("Kohustuslik väli on tühi.");
        }

        Event event = convertToEvent(eventDTO);

        LocalDateTime currentTime = LocalDateTime.now();
        if (event.getTime().isBefore(currentTime)) {
            throw new InvalidInputException("Ürituse aeg saab olla ainult tulevikus.");
        }

        if (event.getInfo().length() > 1000) {
            throw new InvalidInputException("Info maksimaalne pikkus on 1000.");
        }

        if (event.getName().length() > 50) {
            throw new InvalidInputException("Nime maksimaalne pikkus on 50.");
        }

        if (event.getPlace().length() > 200) {
            throw new InvalidInputException("Koha maksimaalne pikkus on 200.");
        }

        repository.save(event);
    }

    private Event convertToEvent(EventDTO eventDTO) {
        Event event = new Event();

        event.setName(eventDTO.getName());
        event.setPlace(eventDTO.getPlace());
        event.setInfo(eventDTO.getInfo());

        LocalDateTime time = LocalDateTime.parse(eventDTO.getTime());
        event.setTime(time);

        return event;
    }

    @Override
    public Event findById(int eventId) {
        Event event = repository.findById(eventId);
        if (event != null) {
            event.setDisplayTime(formatTime(event.getTime()));
        }
        return event;
    }

    @Override
    public List<Event> all() {
        var events = repository.all();
        events.forEach(event -> event.setDisplayTime(formatTime(event.getTime())));
        return events;
    }

    private String formatTime(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return time.format(formatter);
    }

    @Override
    public List<Event> futureEvents() {
        LocalDateTime current = LocalDateTime.now();
        List<Event> events = all().stream().filter(event -> event.getTime().isAfter(current)).toList();
        setParticipantCount(events);
        return events;
    }

    @Override
    public List<Event> pastEvents() {
        LocalDateTime current = LocalDateTime.now();
        List<Event> events = all().stream().filter(event -> event.getTime().isBefore(current)).toList();
        setParticipantCount(events);
        return events;
    }

    private void setParticipantCount(List<Event> events) {
        events.forEach(event -> event.setCount(getParticipantsCount(event.getId())));
    }

    private Integer getParticipantsCount(int eventId) {
        List<Person> people = participantRepository.allPeople(eventId);
        people.forEach(person -> person.setBusiness(false));
        List<Participant> participants = new ArrayList<>(people);

        List<Business> businesses = participantRepository.allBusinesses(eventId);
        businesses.forEach(business -> business.setBusiness(true));
        participants.addAll(businesses);

        return participants.size();
    }

    @Override
    public void removeEvent(int eventId) {
        repository.removeEvent(eventId);
        participantRepository.removeParticipantsByEventId(eventId);
    }
}
