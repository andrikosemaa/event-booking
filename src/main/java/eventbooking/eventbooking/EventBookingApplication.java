package eventbooking.eventbooking;

import eventbooking.eventbooking.config.InitializeDatabase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class EventBookingApplication {

    public static void main(String[] args) throws SQLException {
        new InitializeDatabase("database.db");
        SpringApplication.run(EventBookingApplication.class, args);
    }

}
