package eventbooking.eventbooking.payment.application.service;

import eventbooking.eventbooking.payment.domain.model.PaymentMethod;
import eventbooking.eventbooking.payment.domain.repository.PaymentMethodRepository;
import eventbooking.eventbooking.payment.domain.service.PaymentMethodService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {

    private final @NonNull PaymentMethodRepository repository;

    @Autowired
    public PaymentMethodServiceImpl(@NonNull PaymentMethodRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PaymentMethod> all() {
        return repository.all();
    }
}
