package eventbooking.eventbooking.payment.infrastructure;

import eventbooking.eventbooking.payment.domain.model.PaymentMethod;
import eventbooking.eventbooking.payment.domain.repository.PaymentMethodRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentMethodRepositoryImpl implements PaymentMethodRepository {

    private final @NonNull JdbcTemplate jdbcTemplate;

    @Autowired
    public PaymentMethodRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<PaymentMethod> all() {
        return jdbcTemplate.query("SELECT * FROM payment_methods", BeanPropertyRowMapper.newInstance(PaymentMethod.class));
    }
}
