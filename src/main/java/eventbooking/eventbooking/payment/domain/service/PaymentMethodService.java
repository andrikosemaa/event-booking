package eventbooking.eventbooking.payment.domain.service;

import eventbooking.eventbooking.payment.domain.model.PaymentMethod;

import java.util.List;

public interface PaymentMethodService {

    List<PaymentMethod> all();
}
