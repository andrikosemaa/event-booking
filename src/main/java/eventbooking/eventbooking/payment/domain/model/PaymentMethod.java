package eventbooking.eventbooking.payment.domain.model;

import lombok.Data;

@Data
public class PaymentMethod {
    private String name;
    private String value;
}
