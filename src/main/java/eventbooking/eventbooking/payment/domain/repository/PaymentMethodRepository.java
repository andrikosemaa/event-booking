package eventbooking.eventbooking.payment.domain.repository;

import eventbooking.eventbooking.payment.domain.model.PaymentMethod;

import java.util.List;

public interface PaymentMethodRepository {

    List<PaymentMethod> all();
}
