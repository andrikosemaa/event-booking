package eventbooking.eventbooking.payment.domain.info;

public enum Method {
    TRANSFER("transfer"),
    CASH("cash");

    private String method;

    Method(String method) {
        this.method = method;
    }
}
