package eventbooking.eventbooking.config;

import org.sqlite.SQLiteDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InitializeDatabase {

    SQLiteDataSource dataSource;
    public static final String pathToDatabase = "jdbc:sqlite:src/main/resources/db/";

    public InitializeDatabase(String database) throws SQLException {
        this.dataSource = new SQLiteDataSource();
        this.dataSource.setUrl(pathToDatabase + database);

        createTables();
    }

    void createTables() throws SQLException {
        var connection = dataSource.getConnection();

        createEventsTable(connection);
        createPeopleTable(connection);
        createBusinessesTable(connection);
        createPaymentMethods(connection);
        initializeMethods(connection);

        connection.close();
    }

    private void createEventsTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        statement.execute(
                """
                        CREATE TABLE IF NOT EXISTS events (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name VARCHAR(50) NOT NULL,
                        time VARCHAR(50) NOT NULL,
                        place VARCHAR(200) NOT NULL,
                        info VARCHAR(1000) NOT NULL)"""
        );
    }

    private void createPeopleTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        statement.execute(
                """
                        CREATE TABLE IF NOT EXISTS people (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        event_id INTEGER NOT NULL,
                        firstname VARCHAR(50) NOT NULL,
                        lastname VARCHAR(50) NOT NULL,
                        code INTEGER NOT NULL,
                        method VARCHAR(50) NOT NULL,
                        info VARCHAR(1500) NOT NULL,
                        FOREIGN KEY (event_id) REFERENCES events(id) ON DELETE CASCADE,
                        FOREIGN KEY (method) REFERENCES payment_methods(value))"""
        );
    }

    private void createBusinessesTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        statement.execute(
                """
                        CREATE TABLE IF NOT EXISTS businesses (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        event_id INTEGER NOT NULL,
                        name VARCHAR(100) NOT NULL,
                        code INTEGER NOT NULL,
                        participants INTEGER NOT NULL,
                        method VARCHAR(50) NOT NULL,
                        info VARCHAR(5000) NOT NULL,
                        FOREIGN KEY (event_id) REFERENCES events(id) ON DELETE CASCADE,
                        FOREIGN KEY (method) REFERENCES payment_methods(value))"""
        );
    }

    private void createPaymentMethods(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        statement.execute(
                """
                        CREATE TABLE IF NOT EXISTS payment_methods (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name VARCHAR(50) NOT NULL,
                        value VARCHAR(50) NOT NULL)"""
        );
    }

    private void initializeMethods(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM payment_methods");

        if (resultSet.next() && resultSet.getInt(1) == 0) {
            statement.execute(
                    """
                            INSERT INTO payment_methods (name, value) VALUES 
                            ('Pangaülekanne', 'TRANSFER'),
                            ('Sularaha', 'CASH')
                            """
            );
        }

        resultSet.close();
    }

}
