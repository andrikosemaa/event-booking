const element = document.getElementById("secondary");
element.classList.add("blue");
element.classList.add("text-light");

document.getElementById("back").addEventListener("click", function () {
  window.location.href = "/";
});

document.getElementById("add").addEventListener("click", function () {
  const form = new FormData();
  form.append("name", document.getElementById("name").value);
  form.append("time", document.getElementById("time").value);
  form.append("place", document.getElementById("place").value);
  form.append("info", document.getElementById("info").value);

  fetch("http://localhost:8084/add_event", {
    method: "POST",
    body: form,
  }).then(async (response) => {
    if (response.status == 400) {
      alert(await response.text());
    } else if (response.status == 200) {
      window.location.href = "/";
    } else {
      alert("Failed to add event");
    }
  });
});
