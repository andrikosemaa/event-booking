document.getElementById("back").addEventListener("click", function () {
  window.location.href = "/";
});

document.getElementById("add").addEventListener("click", function () {
  var parts = window.location.href.split("/");
  var eventId = parts[parts.length - 1];
  console.log(eventId);

  const form = new FormData();
  form.append("eventId", eventId);
  form.append("first", document.getElementById("firstname").value);
  form.append("second", document.getElementById("lastname").value);
  form.append("third", document.getElementById("code").value);
  form.append("method", document.getElementById("method").value);
  form.append("info", document.getElementById("info").value);
  form.append(
    "business",
    document.getElementById("business").checked ? true : false
  );

  fetch("http://localhost:8084/add_participant", {
    method: "POST",
    body: form,
  }).then(async (response) => {
    if (response.status == 400) {
      alert(await response.text());
    } else if (response.status == 200) {
      window.location.href = "/";
    } else {
      alert("Failed to add participant");
    }
  });
});

document.getElementById("business").addEventListener("click", function () {
  document
    .querySelectorAll(".person")
    .forEach((person) => (person.style.display = "none"));
  document
    .querySelectorAll(".business")
    .forEach((person) => (person.style.display = "block"));
});

document.getElementById("person").addEventListener("click", function () {
  document
    .querySelectorAll(".person")
    .forEach((person) => (person.style.display = "block"));
  document
    .querySelectorAll(".business")
    .forEach((person) => (person.style.display = "none"));
});

function removePersonEventAndRedirect(personId) {
  fetch("http://localhost:8084/remove_person/" + personId, {
    method: "DELETE",
  }).then(() => {
    window.location.reload();
  });
}

function removeBusinessEventAndRedirect(businessId) {
  fetch("http://localhost:8084/remove_business/" + businessId, {
    method: "DELETE",
  }).then(() => {
    window.location.reload();
  });
}
