document.getElementById("back").addEventListener("click", function () {
  window.location.href = "/";
});

document.getElementById("add").addEventListener("click", function () {
  var parts = window.location.href.split("/");
  var participantId = parts[parts.length - 1];
  var isBusiness = parts[parts.length - 2] == "business";
  console.log(participantId);

  const form = new FormData();
  form.append("id", participantId);
  form.append("eventId", -1);
  form.append("first", document.getElementById("firstname").value);
  form.append("second", document.getElementById("lastname").value);
  form.append("third", document.getElementById("code").value);
  form.append("method", document.getElementById("method").value);
  form.append("info", document.getElementById("info").value);
  form.append("business", isBusiness);

  fetch("http://localhost:8084/update_participant", {
    method: "PUT",
    body: form,
  }).then(async (response) => {
    if (response.status == 400) {
      alert(await response.text());
    } else if (response.status == 200) {
      window.location.href = "/";
    } else {
      alert("Failed to update participant");
    }
  });
});
