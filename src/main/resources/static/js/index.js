const element = document.getElementById("main");
element.classList.add("blue");
element.classList.add("text-light");

function removeEventAndRedirect(eventId) {
  fetch("http://localhost:8084/remove_event/" + eventId, {
    method: "DELETE",
  }).then(() => {
    location.reload();
  });
}
