package eventbooking.eventbooking;

import eventbooking.eventbooking.config.InitializeDatabase;
import eventbooking.eventbooking.events.application.service.EventServiceImpl;
import eventbooking.eventbooking.events.domain.model.Event;
import eventbooking.eventbooking.events.domain.model.EventDTO;
import eventbooking.eventbooking.events.domain.repository.EventRepository;
import eventbooking.eventbooking.events.infrastructure.EventRepositoryImpl;
import eventbooking.eventbooking.participants.application.exception.InvalidInputException;
import eventbooking.eventbooking.participants.application.service.ParticipantServiceImpl;
import eventbooking.eventbooking.participants.domain.model.Business;
import eventbooking.eventbooking.participants.domain.model.ParticipantDTO;
import eventbooking.eventbooking.participants.domain.model.Person;
import eventbooking.eventbooking.participants.domain.repository.ParticipantRepository;
import eventbooking.eventbooking.participants.domain.service.ParticipantService;
import eventbooking.eventbooking.participants.infrastructure.ParticipantRepositoryImpl;
import eventbooking.eventbooking.payment.domain.info.Method;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.sqlite.SQLiteDataSource;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class EventBookingApplicationTests {

	public static final String pathToDirectory = "src/main/resources/db/";

	public static final String databaseName = "testDatabase.db";
	private File database;
	JdbcTemplate template;
	EventRepository eventRepository;
	EventServiceImpl eventService;
	ParticipantService participantService;

	@BeforeEach
	void createTestDatabase() throws IOException, SQLException {
		database = new File(pathToDirectory + databaseName);
		new InitializeDatabase(databaseName);
		database.createNewFile();

		SQLiteDataSource dataSource = new SQLiteDataSource();
		dataSource.setUrl(InitializeDatabase.pathToDatabase + databaseName);
		template = new JdbcTemplate(dataSource);
		eventRepository = new EventRepositoryImpl(template);
		ParticipantRepository participantRepository = new ParticipantRepositoryImpl(template);
		eventService = new EventServiceImpl(eventRepository, participantRepository);
		participantService = new ParticipantServiceImpl(participantRepository);
	}

	@AfterEach
	void deleteTestDatabase() throws IOException {
		if (database.exists()) {
			database.delete();
		}
	}

	@Test
	void testEventService_testAddingEvent_allFieldsShouldMatch() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);
		Event event = eventService.findById(1);

		assertEquals(eventDTO.getName(), event.getName(), "Incorrect name");
		assertEquals(eventDTO.getPlace(), event.getPlace(), "Incorrect name");
		assertEquals(eventDTO.getInfo(), event.getInfo(), "Incorrect name");
	}

	@Test
	void testEventService_testInvalidDate_shouldThrowException() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().minusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		assertThrows(InvalidInputException.class, () -> eventService.addEvent(eventDTO));
	}

	@Test
	void testEventService_testTooLongInfo_shouldThrowException() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("-".repeat(1001));

		assertThrows(InvalidInputException.class, () -> eventService.addEvent(eventDTO));
	}

	@Test
	void testEventService_ifAllGetsRightAmountOfEvents_shouldBeEqualToOne() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);

		assertEquals(1, eventService.all().size());
	}

	@Test
	void testEventService_ifAllGetsRightAmountOfEvents_shouldBeEqualToThree() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);
		eventService.addEvent(eventDTO);
		eventService.addEvent(eventDTO);

		assertEquals(3, eventService.all().size());
	}

	@Test
	void testEventService_ifAllFutureEventsWorksCorrectly_shouldBeEqualToOne() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);

		assertEquals(1, eventService.futureEvents().size());
	}

	@Test
	void testEventService_ifAllPastEventsWorksCorrectly_shouldBeEqualToThree() {
		Event event = new Event();
		event.setName("event");
		event.setTime(LocalDateTime.now().minusDays(1));
		event.setPlace("place");
		event.setInfo("info");

		eventRepository.save(event);

		assertEquals(1, eventService.pastEvents().size());
	}

	@Test
	void testEventService_ifAllPastEventsWorksCorrectly_shouldBeEqualToOne() {
		Event event = new Event();
		event.setName("event");
		event.setTime(LocalDateTime.now().minusDays(1));
		event.setPlace("place");
		event.setInfo("info");

		eventRepository.save(event);
		eventRepository.save(event);
		eventRepository.save(event);

		assertEquals(3, eventService.pastEvents().size());
	}

	@Test
	void testEventService_testIfRemoveEventWorks_shouldBeEqualToZero() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);
		eventService.removeEvent(1);

		assertEquals(0, eventService.all().size());
	}

	@Test
	void testEventService_testIfRemoveEventWorks_shouldBeEqualToTwo() {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setName("event");
		eventDTO.setTime(LocalDateTime.now().plusDays(1).toString());
		eventDTO.setPlace("place");
		eventDTO.setInfo("info");

		eventService.addEvent(eventDTO);
		eventService.addEvent(eventDTO);
		eventService.addEvent(eventDTO);
		eventService.removeEvent(2);

		assertEquals(2, eventService.all().size());
	}


	@Test
	void testParticipantService_testAddPerson_fieldsShouldBeEqual() {
		ParticipantDTO personDTO = new ParticipantDTO();
		personDTO.setFirst("user");
		personDTO.setSecond("name");
		personDTO.setThird("123456789");
		personDTO.setMethod("CASH");
		personDTO.setInfo("info");
		personDTO.setEventId("1");

		participantService.save(personDTO);
		Person person = (Person) participantService.getPersonById(1);

		assertEquals(personDTO.getFirst(), person.getFirstname());
		assertEquals(personDTO.getSecond(), person.getLastname());
		assertEquals(123456789, person.getCode());
		assertEquals(Method.CASH, person.getMethod());
		assertEquals(personDTO.getInfo(), person.getInfo());
		assertEquals(1, person.getEventId());
	}

	@Test
	void testParticipantService_testAddBusiness_fieldsShouldBeEqual() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);

		participantService.save(businessDTO);
		Business business = (Business) participantService.getBusinessById(1);

		assertEquals(businessDTO.getFirst(), business.getName());
		assertEquals(123456789, business.getCode());
		assertEquals(10, business.getParticipants());
		assertEquals(Method.TRANSFER, business.getMethod());
		assertEquals(businessDTO.getInfo(), business.getInfo());
		assertEquals(1, business.getEventId());
	}

	@Test
	void testParticipantService_testGetAll_amountShouldBeEqualToOne() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);

		participantService.save(businessDTO);

		assertEquals(1, participantService.getParticipants(1).size());
	}

	@Test
	void testParticipantService_testGetAll_amountShouldBeEqualToThree() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);

		ParticipantDTO personDTO = new ParticipantDTO();
		personDTO.setFirst("user");
		personDTO.setSecond("name");
		personDTO.setThird("123456789");
		personDTO.setMethod("CASH");
		personDTO.setInfo("info");
		personDTO.setEventId("1");

		participantService.save(businessDTO);
		participantService.save(businessDTO);
		participantService.save(personDTO);

		assertEquals(3, participantService.getParticipants(1).size());
	}

	@Test
	void testParticipantService_testGetAll_amountShouldBeEqualToTwo() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);

		ParticipantDTO personDTO = new ParticipantDTO();
		personDTO.setFirst("user");
		personDTO.setSecond("name");
		personDTO.setThird("123456789");
		personDTO.setMethod("CASH");
		personDTO.setInfo("info");
		personDTO.setEventId("2");

		participantService.save(businessDTO);
		participantService.save(businessDTO);
		participantService.save(personDTO);

		assertEquals(2, participantService.getParticipants(1).size());
	}

	@Test
	void testParticipantService_testRemoveBusiness_amountShouldBeEqualToZero() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);

		participantService.save(businessDTO);
		participantService.removeBusiness(1);

		assertEquals(0, participantService.getParticipants(1).size());
	}

	@Test
	void testParticipantService_testRemovePerson_amountShouldBeEqualToZero() {
		ParticipantDTO personDTO = new ParticipantDTO();
		personDTO.setFirst("user");
		personDTO.setSecond("name");
		personDTO.setThird("123456789");
		personDTO.setMethod("CASH");
		personDTO.setInfo("info");
		personDTO.setEventId("1");

		participantService.save(personDTO);
		participantService.removePerson(1);

		assertEquals(0, participantService.getParticipants(1).size());
	}

	@Test
	void testParticipantService_testUpdatePerson_fieldsShouldBeEqual() {
		ParticipantDTO personDTO = new ParticipantDTO();
		personDTO.setFirst("user");
		personDTO.setSecond("name");
		personDTO.setThird("123456789");
		personDTO.setMethod("CASH");
		personDTO.setInfo("info");
		personDTO.setEventId("1");
		personDTO.setId("1");

		participantService.save(personDTO);

		personDTO.setFirst("user1");
		personDTO.setSecond("name2");
		personDTO.setThird("1234567892");
		personDTO.setMethod("TRANSFER");
		personDTO.setInfo("info2");

		participantService.updateParticipant(personDTO);

		Person person = (Person) participantService.getPersonById(1);

		assertEquals(personDTO.getFirst(), person.getFirstname());
		assertEquals(personDTO.getSecond(), person.getLastname());
		assertEquals(1234567892, person.getCode());
		assertEquals(Method.TRANSFER, person.getMethod());
		assertEquals(personDTO.getInfo(), person.getInfo());
		assertEquals(1, person.getEventId());
	}

	@Test
	void testParticipantService_testUpdateBusiness_fieldsShouldBeEqual() {
		ParticipantDTO businessDTO = new ParticipantDTO();
		businessDTO.setFirst("business");
		businessDTO.setSecond("123456789");
		businessDTO.setThird("10");
		businessDTO.setMethod("TRANSFER");
		businessDTO.setInfo("info");
		businessDTO.setEventId("1");
		businessDTO.setBusiness(true);
		businessDTO.setId("1");

		participantService.save(businessDTO);

		businessDTO.setFirst("business2");
		businessDTO.setSecond("1234567892");
		businessDTO.setThird("12");
		businessDTO.setMethod("CASH");
		businessDTO.setInfo("info2");

		participantService.updateParticipant(businessDTO);

		Business business = (Business) participantService.getBusinessById(1);

		assertEquals(businessDTO.getFirst(), business.getName());
		assertEquals(1234567892, business.getCode());
		assertEquals(12, business.getParticipants());
		assertEquals(Method.CASH, business.getMethod());
		assertEquals(businessDTO.getInfo(), business.getInfo());
		assertEquals(1, business.getEventId());
	}

}
