
# Event Booking

- [About](#about)

- [Structure](#structure)

- [Run the program](#run-the-program)

- [Testing](#testing)

- [Database schema](#schema)

<br>

<a  href="about"></a>

## About

This project is a task for RIK that includes functionality to add events, add participants to events, remove and edit participants and remove events.
Backend is built using java with Spring framework. In addition Thymeleaf is used for templating and Lombok is used to reduce boilerplate code.
For persistence a SQLite database is used for this project.
The frontend is implemented using HTML, CSS and JavaScript. Additionally Bootstrap is used to reduce the amount of CSS required.

<br>

<a  href="structure"></a>

## Structure

This section gives an overview of the structure of the project

The projects consists of three main domains:

	- events
	- participants
	- payment methods

These domains contain all the logic, entities, interfaces and implementations for the specific domain. This way all of the code that is related to a certain domain is contained in that domain. This every domain contains it own controller, service and repository layers.

The entire project is layered in three layers:

	- controller
	- service
	- repository

The layers are ordered in the order shown above. First is controller layer. Then under that is service and under that is repository.
The controller layer includes all of the handlers for different endpoints. The endpoints are for fetching pages and also interacting with the application to add, remove and update different entities.
The service layer includes all of the applications business logic. This is where all of the check for all of the inputs to the application are done as well.
The repository layer is the persistence layer that is used to save and fetch all of the applications data.

<br>

<a  href="run-the-program"></a>

## Run the program

To run the application open the project in IntelliJ and run it. No extra configuration is required to start the application.
If you don't have a run configuration just navigate to the EventBookingApplication class in src/main/java/eventbooking and run the application from this class.
Then you can access te page at: http://localhost:8084/

<br>

<a  href="testing"></a>

## Testing

There are also tests for this project. Currently there are 19 tests. These tests test the service layer.
By testing all of the methods in the service layer it is possible to test the entire applications business logic.
Also since the repository layer is under the service layer then the repository layer is tested by these tests as well.
With this configuration it is possible to test the majority of the application.
Also for each test a dedicated database is created for that specific test meaning that the main database of the application
does not interfere with the testing.

<br>

<a  href="schema"></a>

## Database schema

![database](db_schema.png)